# Bus Route Challenge #

* [GoEuro challenge ](https://github.com/goeuro/challenges/tree/master/bus_route_challenge)
* Version 1.0


### How do I get set up? ###
Start server by 

```
#!bash

bash service.sh start {Input file with bus routes}
```

Check in browser if the route exists

```
#!

http://localhost:8088/api/direct?dep_sid={start stop}&arr_sid={end stop}
```

### Implementation ####

First I implemented routing lookup logic by using mapping of all stations to its route ids (Map<String, Set<String>> stationsWithRouteIds) and mapping of all route ids to its stations in route (Map<String, List<String>>), but that structure was memory intensive and caused failures on big files like was mentioned in the assignment description (100000 x 1000).
My final solution is holding separately routes as list. When request is arriving I'm doing full scan of entries till I find a match or no lines left. 
Of course, that solution having longer latency (up to 1 sec on my machine), but its supporting big files.