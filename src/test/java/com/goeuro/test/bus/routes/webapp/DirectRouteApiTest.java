package com.goeuro.test.bus.routes.webapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.goeuro.test.bus.routes.business.RoutesService;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;


@RunWith(PowerMockRunner.class)
public class DirectRouteApiTest {


    @BeforeClass
    public static void startJetty() throws Exception {
        AppServer.startInTestMode();
    }

    @AfterClass
    public static void stopJetty() {
        AppServer.stop();
    }

    @Test
    public void returnFalseWhenNoDirectRouteFoundTest() throws Exception {

        HttpURLConnection http = (HttpURLConnection)
                new URL("http://localhost:8088/api/direct?dep_sid=114&arr_sid=5555")
                        .openConnection();
        http.connect();
        String body = extractResponseBody(http.getInputStream());

        DirectRouteResponse response = getDirectRouteResponse(body);

        assertThat("Response Code", http.getResponseCode(), is(HttpStatus.OK_200));
        assertThat("Response object", response, notNullValue());
        assertThat("Response arrivalStation", response.arrivalStation, is("5555"));
        assertThat("Response departureStation", response.departureStation, is("114"));
        assertThat("Response directRoute", response.directRoute, is(false));

    }

    @PrepareForTest({ RoutesService.class })
    @Test
    public void returnTrueWhenNoDirectRouteFoundTest() throws Exception {

        PowerMockito.mockStatic(RoutesService.class);
        PowerMockito.when(RoutesService.isDirectRoute("1","2"))
                .thenReturn(true);

        HttpURLConnection http = (HttpURLConnection)
                new URL("http://localhost:8088/api/direct?dep_sid=1&arr_sid=2")
                        .openConnection();
        http.connect();
        String body = extractResponseBody(http.getInputStream());

        DirectRouteResponse response = getDirectRouteResponse(body);

        assertThat("Response Code", http.getResponseCode(), is(HttpStatus.OK_200));
        assertThat("Response object", response, notNullValue());
        assertThat("Response arrivalStation", response.arrivalStation, is("2"));
        assertThat("Response departureStation", response.departureStation, is("1"));
        assertThat("Response directRoute", response.directRoute, is(true));
    }

    @Test
    public void failOnEmptyDepartureAndArrivalStationsTest() throws Exception {

        HttpURLConnection http = (HttpURLConnection)
                new URL("http://localhost:8088/api/direct?dep_sid=&arr_sid=")
                        .openConnection();
        http.connect();
        assertThat("Response Code", http.getResponseCode(), is(HttpStatus.BAD_REQUEST_400));
    }

    @Test
    public void failOnMissingDepartureAndArrivalStationsTest() throws Exception {

        HttpURLConnection http = (HttpURLConnection)
                new URL("http://localhost:8088/api/direct?")
                        .openConnection();
        http.connect();
        assertThat("Response Code", http.getResponseCode(), is(HttpStatus.BAD_REQUEST_400));
    }

    private String extractResponseBody(InputStream inputStream) throws IOException {
        return new BufferedReader(new InputStreamReader(inputStream))
                .lines().collect(Collectors.joining("\n"));
    }

    private DirectRouteResponse getDirectRouteResponse(String body) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(body, DirectRouteResponse.class);
    }

}
