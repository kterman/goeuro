package com.goeuro.test.bus.routes.business;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RoutesServiceTest {
    @Test
    public void directRouteFoundTest(){
        List<String> routes = new ArrayList<>(Arrays.asList("1 2 3 4", "4 5 6 3"));
        RoutesService.setRoutes(routes);

        assertTrue(RoutesService.isDirectRoute("6", "3"));
    }

    @Test
    public void directRouteNotFoundTest(){
        List<String> routes = new ArrayList<>(Arrays.asList("1 2 3 4", "4 5 6 3"));
        RoutesService.setRoutes(routes);

        assertFalse(RoutesService.isDirectRoute("3", "6"));
    }

    @Test
    public void sameStartEndStationsTest(){
        List<String> routes = new ArrayList<>(Collections.singletonList("1 2 3 4"));
        RoutesService.setRoutes(routes);

        assertFalse(RoutesService.isDirectRoute("2", "2"));
    }

    @Test
    public void startStationAfterEndStationTest(){
        List<String> routes = new ArrayList<>(Collections.singletonList("1 2 3 4"));
        RoutesService.setRoutes(routes);

        assertFalse(RoutesService.isDirectRoute("3", "2"));
    }

    @Test
    public void stationIdAsRouteIdTest(){
        List<String> routes = new ArrayList<>(Collections.singletonList("1 2 3 4"));
        RoutesService.setRoutes(routes);

        assertFalse(RoutesService.isDirectRoute("1", "2"));
    }

    @Test
    public void requestStationIdLikeAnotherIdInRouteTest(){
        List<String> routes = new ArrayList<>(Collections.singletonList("1 2 33 4"));
        RoutesService.setRoutes(routes);

        assertFalse(RoutesService.isDirectRoute("2", "3"));
    }
}
