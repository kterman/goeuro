package com.goeuro.test.bus.routes.business;

import com.goeuro.test.bus.routes.business.error.ValidationRouteException;
import org.junit.Test;

import java.net.URISyntaxException;

public class RoutesFileParserTest {


    @Test (expected = RuntimeException.class)
    public void readNotExistingFileNameTest() {
        RoutesFileParser.readRoutesFile("NotExistingFile");
    }

    @Test (expected = ValidationRouteException.class)
    public void numberOfRoutesNotValid() throws URISyntaxException {
        String fileName = ClassLoader.getSystemResource("bus_route_1").getFile();
        RoutesFileParser.readRoutesFile(fileName);
    }

    @Test (expected = ValidationRouteException.class)
    public void duplicateRouteIdsTest() throws URISyntaxException {
        String fileName = ClassLoader.getSystemResource("bus_route_2").getFile();
        RoutesFileParser.readRoutesFile(fileName);
    }
    @Test (expected = ValidationRouteException.class)
    public void duplicateStationsInRouteTest() throws URISyntaxException {
        String fileName = ClassLoader.getSystemResource("bus_route_3").getFile();
        RoutesFileParser.readRoutesFile(fileName);
    }



}
