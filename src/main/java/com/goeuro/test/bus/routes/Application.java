package com.goeuro.test.bus.routes;

import com.goeuro.test.bus.routes.business.RoutesFileParser;
import com.goeuro.test.bus.routes.webapp.AppServer;

public class Application {
    public static void main(String[] args) {
        RoutesFileParser.readRoutesFile(args[0]);

        AppServer.bootstrap();
    }
}
