package com.goeuro.test.bus.routes.business;

import java.util.*;

public class RoutesService {

    private static RoutesService ourInstance = new RoutesService();

    private static List<String> routes;

    public static RoutesService getInstance() {
        return ourInstance;
    }

    private RoutesService() {
        routes = new ArrayList<>();
    }

    public static void setRoutes(List<String> routes) {
        routes.replaceAll(route -> route += " ");
        RoutesService.routes = routes;
    }

    public static boolean isDirectRoute(String startStation, String endStation) {

        for (String route : routes) {
            if (find(startStation, endStation, route)) {
                return true;
            }
        }
        return false;
    }


    private static boolean find(String startStation, String endStation, String route) {
        int start = route.indexOf(" " + startStation + " ");
        int end = route.indexOf(" " + endStation + " ");

        return (start > 0 && end > 0 && start < end);
    }
}
