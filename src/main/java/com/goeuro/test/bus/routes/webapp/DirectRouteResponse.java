package com.goeuro.test.bus.routes.webapp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DirectRouteResponse {

    @JsonProperty("dep_sid")
    public String departureStation;

    @JsonProperty("arr_sid")
    public String arrivalStation;

    @JsonProperty("direct_bus_route")
    public boolean directRoute;

    public DirectRouteResponse() {
    }

    public DirectRouteResponse(String departureStation, String arrivalStation, boolean directRoute) {
        this.departureStation = departureStation;
        this.arrivalStation = arrivalStation;
        this.directRoute = directRoute;
    }
}
