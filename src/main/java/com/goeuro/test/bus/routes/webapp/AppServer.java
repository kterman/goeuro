package com.goeuro.test.bus.routes.webapp;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

public class AppServer {

    private static Server server;

    public static void bootstrap() {

        configureServer();
        startServer();
    }

    private static void startServer() {
        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            server.destroy();
        }
    }

    private static void configureServer() {
        ResourceConfig resourceConfig = new ResourceConfig();
        resourceConfig.packages("com.goeuro.test.bus.routes");
        resourceConfig.register(JacksonFeature.class);

        ServletHolder jerseyServlet
                = new ServletHolder(new ServletContainer(resourceConfig));

        server = new Server(8088);

        ServletContextHandler context = new ServletContextHandler(server, "/");
        context.addServlet(jerseyServlet, "/*");
    }

    public static void stop (){
        try {
            server.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void startInTestMode () throws Exception {
        configureServer();
        server.start();
    }
}
