package com.goeuro.test.bus.routes.business.error;

public class ValidationRouteException extends RuntimeException {

    public ValidationRouteException(String message) {
        super(message);
    }

    public ValidationRouteException(String message, Exception e) {
        super(message, e);
    }
}
