package com.goeuro.test.bus.routes.webapp;

import com.goeuro.test.bus.routes.business.RoutesService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/api/direct")
public class DirectRouteApi {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response isDirectRoute(@QueryParam("dep_sid") String dep, @QueryParam("arr_sid") String arr) {
        if (dep == null || dep.isEmpty() || arr == null || arr.isEmpty()) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        DirectRouteResponse response = new DirectRouteResponse(dep, arr, RoutesService.isDirectRoute(dep, arr));
        return Response.ok().entity(response).build();
    }

}

