package com.goeuro.test.bus.routes.business;

import com.goeuro.test.bus.routes.business.error.ValidationRouteException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RoutesFileParser {
    public static void readRoutesFile(String fileName) {

        long start = System.currentTimeMillis();
        try {
            List<String> fileLines = Files.readAllLines(Paths.get(fileName));

            validateNumberOfRoutes(fileLines);
            validateRouteUniqueness(fileLines);

            fileLines.remove(0);
            RoutesService.setRoutes(fileLines);

        } catch (IOException e) {
            throw new RuntimeException("Error reading routes file", e);
        }
        System.err.println(System.currentTimeMillis() - start + "ms");
    }

    public static void validateNumberOfRoutes(List<String> lines) {
        if (lines.isEmpty()){
            throw new ValidationRouteException("The routes file is empty");
        }
        try {
            Integer numberOfRoutes = new Integer(lines.get(0));
            if (numberOfRoutes != lines.size() - 1) {
                throw new ValidationRouteException("Invalid number of routes in file, expected " + numberOfRoutes +
                        " but was " + (lines.size() - 1));
            }
        }catch (Exception e){
            throw new ValidationRouteException("Invalid routes file format", e);
        }
    }

    public static void validateRouteUniqueness(List<String> lines) {
        int numberOfRoutes = lines.size() - 1;
        List<String> routeIds = new ArrayList<>(numberOfRoutes);
        for (int i = 1; i <= numberOfRoutes; i++) {

            String[] routeLineAsArray = lines.get(i).split(" ");

            List<String> routeLine = Arrays.stream(routeLineAsArray).collect(Collectors.toList());
            String routeId = routeLine.remove(0);

            routeIds.add(routeId);

            Set<String> routeAsSet = routeLine.stream().collect(Collectors.toSet());

            validateDuplicateStations(i, routeLineAsArray, routeAsSet);
        }
        validateRouteIdsAreUnique(routeIds);

    }

    private static void validateRouteIdsAreUnique(List<String> routeIds) {
        if (routeIds.size() != routeIds.stream().collect(Collectors.toSet()).size()) {
            throw new ValidationRouteException("The file includes duplicate routes");
        }
    }

    private static void validateDuplicateStations(int i, String[] routeLineAsArray, Set<String> routeAsSet) {
        if (routeAsSet.size() != routeLineAsArray.length - 1) {
            throw new ValidationRouteException("The routes " + i + " has duplicate stations");
        }
    }
}


